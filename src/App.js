import './App.css';
import { Route, Routes, BrowserRouter as Router } from "react-router-dom";
import LoginPage from './pages/LoginPage';
import HomePage from './pages/HomePage';
import SignUpPage from './pages/SignUpPage';
import ProductsPage from './pages/ProductsPage';
import 'bootstrap/dist/css/bootstrap.min.css';
import RecipesPage from './pages/RecipesPage';
import Navigation from "./components/Navigation";
import TokenManager from "./apis/TokenManager";
import { useState, useEffect } from "react";
import CreateRecipePage from './pages/CreateRecipePage';
import ConsumerAPI from './apis/ConsumerAPI';
import CompanyAPI from './apis/CompanyAPI';
import { Client } from '@stomp/stompjs';
import { v4 as uuidv4 } from 'uuid';
import ChatMessagesPlaceholder from './components/ChatMessagesPlaceHolder';
import RecipeDetailsPage from './pages/RecipeDetailsPage';

function App() {

  const [claims, setClaims] = useState(TokenManager.getClaims());
  const [stompClient, setStompClient] = useState();
  const [username, setUsername] = useState("");
  const [messagesReceived, setMessagesReceived] = useState([]);

  const setupStompClient = (username) => {
    // stomp client over websockets
    const stompClient = new Client({
      brokerURL: 'ws://localhost:8080/ws',
      reconnectDelay: 5000,
      heartbeatIncoming: 4000,
      heartbeatOutgoing: 4000
    });

    stompClient.onConnect = () => {
      // subscribe to the backend public topic
      stompClient.subscribe('/topic/publicmessages', (data) => {
        console.log(data);
        onMessageReceived(data);
      });

      // subscribe to the backend "private" topic
      stompClient.subscribe(`/user/${username}/queue/inboxmessages`, (data) => {
        onMessageReceived(data);
      });
    };

    // initiate client
    stompClient.activate();

    // maintain the client for sending and receiving
    setStompClient(stompClient);
    console.log(stompClient);
  };

  // send the data using Stomp
  const sendMessage = (newMessage) => {
    console.log("Sending new Message", newMessage);
    const payload = { 'id': uuidv4(), 'from': username, 'to': newMessage.to, 'text': newMessage.text };
    if (payload.to) {
      stompClient.publish({ 'destination': `/user/${payload.to}/queue/inboxmessages`, body: JSON.stringify(payload) });
    } else {
      stompClient.publish({ 'destination': '/topic/publicmessages', body: JSON.stringify(payload) });
    }
  };

  // display the received data
  const onMessageReceived = (data) => {
    const message = JSON.parse(data.body);
    setMessagesReceived(messagesReceived => [...messagesReceived, message]);
  };

  useEffect(() => {

    const userId = TokenManager.getClaims()?.roleId;
    const role = TokenManager.getClaims()?.role;

    if (role == 'CONSUMER' && userId) {
      ConsumerAPI.getConsumer(userId)
      .then(consumer => {setUsername(consumer.account.email)})
      .catch(error => console.error(error));
    }
    else if (role == 'COMPANY' && userId) {
      CompanyAPI.getCompany(userId)
      .then(company => {setUsername(company.account.email)})
      .catch(error => console.error(error));
    }
  }, [claims]);

  useEffect(() => {
    if (username != "") {
      setupStompClient(username);
    }
  }, [username]);

  return (
    <div className="App">
      <Navigation claims={claims} setClaims={setClaims}/>

      <Router>
        <Routes>
          <Route path="/" element={<HomePage claims={claims}/>} />
          <Route path="/login" element={<LoginPage claims={claims} setClaims={setClaims}/>} />
          <Route path="/signup" element={<SignUpPage />} />
          <Route path="/products" element={<ProductsPage claims={claims}/>} />
          <Route path="/recipes" element={<RecipesPage claims={claims}/>} />
          <Route path="/create-recipe" element={<CreateRecipePage onMessageSend={sendMessage}/>} />
          <Route path="/notifications" element={<ChatMessagesPlaceholder username={username} messagesReceived={messagesReceived} />} />
          <Route path="/recipes/:recipeId" element={<RecipeDetailsPage />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
