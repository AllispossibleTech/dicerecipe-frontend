import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import RecipesApi from "../apis/RecipesApi";

function RecipeDetailsPage() {
  const { recipeId } = useParams();
  const [recipe, setRecipe] = useState(null);

  useEffect(() => {
    const fetchRecipe = async () => {
      try {
        const recipeData = await RecipesApi.getRecipe(recipeId);
        setRecipe(recipeData);
        console.log("recipeData", recipeData);
      } catch (error) {
        console.log(error);
      }
    };

    fetchRecipe();
  }, [recipeId]);

  if (!recipe) {
    return <div>Loading...</div>;
  }

  return (
    <div className="container">
      <div class="row">
        <br></br>
      </div>
      <div class="row">

        <div class="col-2">
          <b><u>Recipe details:</u></b>
          <ul className="list-group mt-3">
            <li className="list-group-item d-flex justify-content-between align-items-center"><b>Name:</b> {recipe.title}</li>
            <li className="list-group-item d-flex justify-content-between align-items-center"><b>Added by:</b> {recipe.companyDetails.name}</li>
            <li className="list-group-item d-flex justify-content-between align-items-center"><b>Time to Prepare:</b> {recipe.timeToPrepare} minutes</li>
          </ul>
        </div>

        <div class="col-8">
          <b><u>Description:</u></b>
          <br></br><br></br>
          <div class="border rounded" style={{paddingTop:'5px', paddingBottom:'5px'}}>
            {recipe.description}</div>
        </div>

        <div class="col-2">
          <b><u>Ingredients:</u></b>
          <ul className="list-group mt-3">
            {recipe.ingredients.map((product) => (
              <li className="list-group-item d-flex justify-content-between align-items-center" key={product.id}>
                {product.name}
              </li>
            ))}
          </ul>
        </div>

      </div>
    </div>
  );
}

export default RecipeDetailsPage;