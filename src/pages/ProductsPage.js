import React, { useState, useEffect } from 'react';
import ProductsApi from '../apis/ProductsApi';
import ConsumerAPI from '../apis/ConsumerAPI';
import CompanyAPI from '../apis/CompanyAPI';
import AddProductForm from '../components/AddProductForm';
import { Popover, OverlayTrigger, Button , Badge} from "react-bootstrap"

function ProductsPage(props) {
  const [products, setProducts] = useState([]);
  const [userDetails, setUserDetails] = useState(null);
  const [searchQuery, setSearchQuery] = useState('');

  const getProductsList = () => {
    console.log("fetching products...");

    ProductsApi.getProducts()
      .then(products => setProducts(products))
      .catch(error => console.log(error));
  }


  useEffect(() => {
    console.log("refreshing page");
    getProductsList();
    getUserDetails();
  }, []);

    const getUserDetails = () => {

        if (props.claims?.role == 'CONSUMER' && props.claims?.userId) {
            ConsumerAPI.getConsumer(props.claims.roleId)
            .then(consumer => setUserDetails(consumer))
            .catch(error => console.error(error));
        }
        else if (props.claims?.role == 'COMPANY' && props.claims?.userId) {
            CompanyAPI.getCompany(props.claims.roleId)
            .then(company => setUserDetails(company))
            .catch(error => console.error(error));
        }
    }

    const handleAddToInventory = (productId) => async () => {
        await ConsumerAPI.addProductToFoodInventory(productId, userDetails.id);
        window.location.reload();
    }

    const handleSearchProducts = () => {
      if (searchQuery.trim() === "") {
        getProductsList();
      } else {
        ProductsApi.getSearchedProducts(searchQuery)
          .then(products => setProducts(products))
          .then(console.log(products))
          .catch(error => console.log(error));
      }
    }

    const handleAddProduct = () => {
      getProductsList();
      window.location.reload();
    }

    const popover = (
      <Popover>
        <Popover.Body>This product is already in your inventory!</Popover.Body>
      </Popover>
    );

    if (props.claims?.role == 'CONSUMER' && userDetails != null)
    {
        return (
          <div className="container mt-5">
            <div className="d-flex justify-content-between align-items-center">
              <h2>Food Products</h2>
              <div className="input-group w-25">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Search"
                  value={searchQuery}
                  onChange={event => setSearchQuery(event.target.value)}
                />
                <button
                  className="btn btn-outline-secondary"
                  type="button"
                  onClick={handleSearchProducts}
                >
                  Go
                </button>
              </div>
            </div>
            <ul className="list-group mt-3">
              {products.map(product => (
                <li className="list-group-item d-flex justify-content-between align-items-center" key={product.id}>
                  {product.name} - Produced by {product.manufacturer.name} at {product.manufacturer.address}
                  {userDetails.foodInventory.some(item => item.id === product.id) ? (
                    <OverlayTrigger trigger={["hover", "focus"]} placement="left" overlay={popover}>
                      <div style={{ display: 'inline-block' }}>
                        <Button className="btn btn-primary btn-sm" disabled>Add to Inventory</Button>
                      </div>
                    </OverlayTrigger>
                    ) : (
                      <button className="btn btn-primary btn-sm" onClick={handleAddToInventory(product.id)}>Add to Inventory</button>
                    )}
                </li>
              ))}
            </ul>
          </div>
        )

    } else if (props.claims?.role == 'COMPANY' && userDetails != null) {
        return (
          <div className="container mt-5">
            <div className="row">
              <div className="col-md-8">
                <h2>All Food Products</h2>
                <div className="input-group mb-3">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Search"
                    value={searchQuery}
                    onChange={event => setSearchQuery(event.target.value)}
                  />
                  <button
                    className="btn btn-outline-secondary"
                    type="button"
                    onClick={handleSearchProducts}
                  >
                    Go
                  </button>
                </div>
                <ul className="list-group mt-3">
                  {products.map(product => (
                    <li className="list-group-item d-flex justify-content-between align-items-center" key={product.id}>
                      {product.name} - Produced by {product.manufacturer.name} at {product.manufacturer.address}
                    </li>
                  ))}
                </ul>
              </div>
              <div className="col-md-4">
                <AddProductForm id={props.claims.roleId} handleAddProduct={handleAddProduct}/>
              </div>
                
            </div>
          </div>
        )
    }

  return (
    <div className="container mt-5">
      <div className="d-flex justify-content-between align-items-center">
        <h2>Food Products</h2>
        <div className="input-group w-25">
          <input
            type="text"
            className="form-control"
            placeholder="Search"
            value={searchQuery}
            onChange={event => setSearchQuery(event.target.value)}
          />
          <button
            className="btn btn-outline-secondary"
            type="button"
            onClick={handleSearchProducts}
          >
            Go
          </button>
        </div>
      </div>
      <ul className="list-group mt-3">
        {products.map(product => (
          <li className="list-group-item d-flex justify-content-between align-items-center" key={product.id}>
            {product.name} - Produced by {product.manufacturer.name} at {product.manufacturer.address}
          </li>
        ))}
      </ul>
    </div>
  );
}

export default ProductsPage;