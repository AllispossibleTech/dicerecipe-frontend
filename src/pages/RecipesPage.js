import React, { useState, useEffect } from 'react';
import RecipesApi from '../apis/RecipesApi.js';
import { useNavigate } from "react-router-dom";
import ConsumerAPI from '../apis/ConsumerAPI.js';

function RecipesPage(props) {
  const [recipes, setRecipes] = useState([]);
  const navigate = useNavigate();


  const [searchMin, setSearchMin] = useState(0);
  const [searchMax, setSearchMax] = useState(0);

  const getRecipesList = () => {
    RecipesApi.getRecipes()
      .then(recipes => setRecipes(recipes))
      .catch(error => console.log(error))
      .then(console.log(recipes));
  }

  const handleApiCall = async () => {
    if (props.claims && props.claims?.role === "CONSUMER") {
      let consumer = null;
  
      try {
        consumer = await ConsumerAPI.getConsumer(props.claims?.roleId);
        if (consumer != null) {
          const preferredTime = consumer.preferredCookingTime;
          console.log(preferredTime);
          RecipesApi.getTimeToPrepare(preferredTime)
            .then(recipes => setRecipes(recipes))
            .catch(error => console.log(error));
        }
      } catch (error) {
        console.log(error);
      }
    }
  };

  const handleApiRangeCall = async () => {
    await RecipesApi.getTimeToPrepareRange(searchMin, searchMax)
      .then(recipes => setRecipes(recipes))
      .catch(error => console.log(error));
  };

  useEffect(() => {
    getRecipesList();
  }, []);

  useEffect(() => {
  }, [recipes]);

  if (props.claims && props.claims?.role == "CONSUMER") {
    const consumer = ConsumerAPI.getConsumer(props.claims?.roleId);
    if (consumer) {
      return (
        <div className="container mt-5">
        <div className="row justify-content-center">
          <div className="col-5">
            <h2>Recipes</h2>
            <br></br>
            <div className='row'>
              <div className='col-4'>
                <div>Filter by time to prepare (minutes):</div>
              </div>
              <div className='col-6'>
              <input
                        type="number"
                        className="form-control"
                        placeholder="Min"
                        onChange={event => setSearchMin(event.target.value)}
                      />
              <input
                        type="number"
                        className="form-control"
                        placeholder="Max"
                        onChange={event => setSearchMax(event.target.value)}
                      />
              </div>
              <div className='col-2'>
                <button style={{width: "100%", marginBottom:"5px"}} className="btn btn-primary btn-sm float-right" onClick={handleApiRangeCall}>Search</button>
                <button style={{width: "100%"}} className="btn btn-secondary btn-sm float-right" onClick={getRecipesList}>Reset</button>
              </div>
            </div>
            <br></br>
            <div className='row' style={{display:"flex", alignItems: "center", textAlign:'center', alignContent:"center"}}>
              <div className='col-12'>
                <button style={{width:"60%", marginBottom: "5px"}} className="btn btn-primary btn-sm float-right" onClick={handleApiCall}>Filter by Your Preferred Cooking Time</button>
                <button style={{width:"60%"}} className="btn btn-secondary btn-sm float-right" onClick={getRecipesList}>Reset</button>
              </div>
            </div>
            
            <ul className="list-group mt-3">
              {recipes.map(recipe => (
                <li className="list-group-item d-flex justify-content-between align-items-center" key={recipe.id}>
                  {recipe.title}
                  <button className="btn btn-primary btn-sm" onClick={() => navigate(`/recipes/${recipe.id}`)}>View</button>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
      )
    }
  }

  return (
    <div className="container mt-5">
      <div className="row justify-content-center">
        <div className="col-5">
          <h2>Recipes</h2>
          <br></br>
          <div className='row'>
            <div className='col-4'>
              <div>Filter by time to prepare (minutes):</div>
            </div>
            <div className='col-6'>
            <input
                      type="number"
                      className="form-control"
                      placeholder="Min"
                      onChange={event => setSearchMin(event.target.value)}
                    />
            <input
                      type="number"
                      className="form-control"
                      placeholder="Max"
                      onChange={event => setSearchMax(event.target.value)}
                    />
            </div>
            <div className='col-2'>
              <button style={{width: "100%", marginBottom:"5px"}} className="btn btn-primary btn-sm float-right" onClick={handleApiRangeCall}>Search</button>
              <button style={{width: "100%"}} className="btn btn-secondary btn-sm float-right" onClick={getRecipesList}>Reset</button>
            </div>
          </div>
          
          <ul className="list-group mt-3">
            {recipes?.map(recipe => (
              <li className="list-group-item d-flex justify-content-between align-items-center" key={recipe.id}>
                {recipe.title}
                <button className="btn btn-primary btn-sm" onClick={() => navigate(`/recipes/${recipe.id}`)}>View</button>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
}

export default RecipesPage;