import React, { useState, useEffect } from 'react';
import ProductsApi from '../apis/ProductsApi';
import RecipesApi from '../apis/RecipesApi';
import CompanyAPI from '../apis/CompanyAPI';
import { Form, Button, Container, Row, Col, Modal } from 'react-bootstrap';
import TokenManager from '../apis/TokenManager';

const CreateRecipePage = (props) => {
    
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [timeToPrepare, setTimeToPrepare] = useState(0);
    const [ingredients, setIngredients] = useState([]);
    const [products, setProducts] = useState([]);
    const [loading, setLoading] = useState(true);
    const [companyDetails, setCompanyDetails ] = useState(null)
    const [show, setShow] = useState(false);
    const [creationInfo, setCreationInfo] = useState(null)

    // handleClose
    const handleClose = () => {setShow(false); setCreationInfo(null)}

    useEffect(() => {
      fetchDetails();
    }, []);

    useEffect(() => {
      fetchProducts();
    }, [companyDetails])

    const fetchDetails = async () => {
      await CompanyAPI.getCompany(TokenManager.getClaims()?.roleId)
        .then(response => {setCompanyDetails(response.details)})
        .catch(error => {console.log(error)});
    }

    const fetchProducts = async () => {
      if (companyDetails) {
        await ProductsApi.getProductsByCompanyDetailsId(companyDetails?.id)
        .then(data => {
        setProducts(data);
        setLoading(false);
        })
        .catch(error => {
        console.error('Error fetching products:', error);
        setLoading(false);
        });
      }
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const recipe = {
            title,
            description,
            timeToPrepare,
            ingredients,
            companyDetails
        };

        await RecipesApi.createRecipe(recipe)
        .then(recipeId => {
            console.log('Recipe created successfully. Recipe ID:', recipeId);
            resetForm();
            setCreationInfo(recipeId);
            setShow(true);
        })
        .then(props.onMessageSend({ 'text': "New Recipe Available: "+recipe.title, 'to': '' }))
        .catch(error => {
            console.error('Error creating recipe:', error);
        });
    };

    const resetForm = () => {
        setTitle('');
        setDescription('');
        setTimeToPrepare(0);
        setIngredients([]);
      };

    const handleIngredientChange = (event) => {
        const ingredientId = parseInt(event.target.value);
        const isChecked = event.target.checked;

        if (isChecked) {
            setIngredients((prevIngredients) => {
            const updatedIngredients = [...prevIngredients];
            const ingredient = products.find((product) => product.id === ingredientId);
            if (ingredient) {
                updatedIngredients.push(ingredient);
            }
            return updatedIngredients;
            });
        } else {
            setIngredients((prevIngredients) => {
            const updatedIngredients = prevIngredients.filter((ingredient) => ingredient.id !== ingredientId);
            return updatedIngredients;
            });
        }
    };

    if (companyDetails) {
      return (
        <div className="d-flex align-items-center justify-content-center">
        <br></br><br></br><br></br>

        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Recipe Created</Modal.Title>
          </Modal.Header>
          <Modal.Body>{creationInfo}!</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>

        <div>
          <h1>Create Recipe</h1>
          <Container>
            <Form onSubmit={handleSubmit}>
              <Row>
                <Col>
                  <Form.Group controlId="title">
                    <Form.Label>Title</Form.Label>
                    <Form.Control
                      type="text"
                      value={title}
                      onChange={event => setTitle(event.target.value)}
                    />
                  </Form.Group>
                </Col>
              </Row>
        
        <Row>
          <Col md={12}>
            <Form.Group controlId="description">
              <Form.Label>Description</Form.Label>
              <Form.Control
                type="text"
                value={description}
                onChange={event => setDescription(event.target.value)}
              />
            </Form.Group>
          </Col>
        </Row>

        <Row>
          <Col md={12}>
            <Form.Group controlId="timeToPrepare">
              <Form.Label>Time to Prepare (minutes)</Form.Label>
              <Form.Control
                type="number"
                value={timeToPrepare}
                onChange={event => setTimeToPrepare(parseInt(event.target.value))}
              />
            </Form.Group>
          </Col>
        </Row>

        <Row>
          <Col md={12}>
            <Form.Group>
              <Form.Label>Ingredients</Form.Label>
              {loading ? (
                <p>Loading...</p>
              ) : (
                <ul>
                  {products?.map((product) => (
                    <li key={product.id}>
                      <Form.Check
                        type="checkbox"
                        id={`ingredient-${product.id}`}
                        label={product.name}
                        checked={ingredients.some((ingredient) => ingredient.id === product.id)}
                        onChange={handleIngredientChange}
                        value={product.id}
                      />
                    </li>
                  ))}
                </ul>
              )}
            </Form.Group>
          </Col>
        </Row>

        <Row>
            <Col>
              <Button variant="primary" type="submit">Create Recipe</Button>
            </Col>
          </Row>
        </Form>
      </Container>
    </div>
    </div>
    );
  } else {
    return(
      <div>You cannot access this page if you're not logged in!</div>
    )
  }


    
};

export default CreateRecipePage;