import React, { useState } from "react";
import LoginForm from "../components/LoginForm";
import ConsumerAPI from "../apis/ConsumerAPI";
import TokenManager from "../apis/TokenManager";
import AuthAPI from "../apis/AuthAPI";
import ConsumerDetails from "../components/ConsumerDetails";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

function LoginPage(props) {

    const [consumerDetails, setConsumerDetails] = useState(null);
    const navigate = useNavigate();
    const delay = ms => new Promise(res => setTimeout(res, ms));

    const handleLogin = async (email, password) => {
        AuthAPI.login(email, password)
          .catch(() => alert("Login failed!"))
          .then(claims => props.setClaims(claims))
          .then(await delay(1000))
          .then(navigate("/"))
          .catch(error => console.error(error));
    }

    const getConsumerDetails = () => {
        const claims = TokenManager.getClaims();

        if (claims?.role == 'CONSUMER' && claims?.userId) {
            ConsumerAPI.getConsumer(claims.roleId)
            .then(consumer => setConsumerDetails(consumer))
            .catch(error => console.error(error));
        }
    }

    useEffect(() => {
        getConsumerDetails();
      }, [props.claims]);

    return (
    <div>
        {props.claims ? (
            <div>
            <p>Login Successfull. </p> <div>Redirecting to HomePage...</div>
            </div>
        ) : (
            <LoginForm onLogin={handleLogin} />
        )}
    </div>
    )
}

export default LoginPage;