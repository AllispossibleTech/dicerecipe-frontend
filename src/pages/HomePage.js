import React, { useState, useEffect } from "react";
import ConsumerDetails from "../components/ConsumerDetails";
import CompanyDetails from "../components/CompanyDetails";
import ConsumerAPI from "../apis/ConsumerAPI";
import CompanyAPI from "../apis/CompanyAPI";
import ProductsApi from "../apis/ProductsApi";
import FoodInventoryList from "../components/FoodInventoryList";
import SuggestedRecipesList from "../components/SuggestedRecipesList";
import ProductsByCompanyList from "../components/ProductsByCompanyList";
import RecipesByCompanyList from "../components/RecipesByCompanyList";

function HomePage(props) {

    const [userDetails, setUserDetails] = useState(null);
    
    // These states will be moved to a <ConsumerView/> Component.
    // const [foodInventory, setFoodInventory] = useState(null);
    // const [recipeSuggestions, setRecipeSuggestions] = useState(null);

    useEffect(() => {
        getUserDetails();
      }, []);

    const getUserDetails = () => {

        if (props.claims?.role == 'CONSUMER' && props.claims?.userId) {
            ConsumerAPI.getConsumer(props.claims.roleId)
            .then(consumer => setUserDetails(consumer))
            .catch(error => console.error(error));
        }
        else if (props.claims?.role == 'COMPANY' && props.claims?.userId) {
            CompanyAPI.getCompany(props.claims.roleId)
            .then(company => setUserDetails(company))
            .catch(error => console.error(error));
        }
    }

    const handlRemoveFromInventory = async (productId) => {
        await ConsumerAPI.removeProductFromFoodInventory(productId, userDetails.id);
        getUserDetails();
        window.location.reload();
    }

    if (props.claims?.role == 'CONSUMER' && userDetails != null)
    {
        return (
            <div className="container">
                <div class="row">
                    <br></br>
                </div>
                <div class="row">
                    <div class="col-4">
                        <b><u>Your details:</u></b>
                        <ConsumerDetails consumerDetails={userDetails}/>
                    </div>
                    <div class="col-4">
                        <b><u>Your food inventory:</u></b>
                        <FoodInventoryList products={userDetails.foodInventory} removeFromInventory={handlRemoveFromInventory}/>
                    </div>
                    <div class="col-4">
                        <b><u>Suggested Recipes:</u></b>
                        <SuggestedRecipesList consumerId={userDetails.id}/>
                    </div>
                </div>
            </div>
        )
    } else if (props.claims?.role == 'COMPANY' && userDetails != null) {
        return (
            <div className="container">
                <div class="row">
                    <br></br>
                </div>
                <div class="row">
                    <div class="col-4">
                        <b><u>Your details:</u></b>
                        <CompanyDetails companyDetails={userDetails}/>
                    </div>
                    <div class="col-4">
                        <b><u>Your Products:</u></b>
                        <ProductsByCompanyList detailsId={userDetails.details.id} getUserDetails={getUserDetails}/>
                    </div>
                    <div class="col-4">
                        <b><u>Your Recipes:</u></b>
                        <RecipesByCompanyList detailsId={userDetails.details.id} getUserDetails={getUserDetails}/>
                    </div>
                </div>
            </div>
        )
    }

    return (
        <div className="container text-center mt-5">
      <h2 className="display-4 mb-4">Welcome to DiceRecipe!</h2>
      <div className="card bg-light mb-5">
        <div className="card-body">
          <h5 className="card-title">Discover Exciting Recipes</h5>
          <p className="card-text">
            <strong>DiceRecipe</strong> is an innovative recipe suggestion application that helps you discover exciting and delicious recipes based on the food products you already have in your kitchen.
          </p>
          <p className="card-text">
            Say goodbye to the question of "What should I cook today?" as <strong>DiceRecipe</strong> utilizes advanced algorithms to analyze your food inventory and provides personalized recipe recommendations tailored to your taste preferences and dietary restrictions.
          </p>
          <p className="card-text">
            With <strong>DiceRecipe</strong>, you'll never run out of ideas for your next meal and can easily explore new culinary adventures from the comfort of your home. Join us today and embark on a delightful cooking journey!
          </p>
        </div>
      </div>
      <p className="lead">Login with your account to get started!</p>
    </div>
    )
}

export default HomePage;