import React, { useState } from "react";
import { ButtonGroup, ToggleButton } from "react-bootstrap";
import CompanySignUp from "../components/CompanySignUp";
import ConsumerSignUp from "../components/ConsumerSignUp";

function SignUpPage() {

    const [selectedRole, setSelectedRole] = useState('1');

    const usersRoles = [
        { name: 'Consumer', value: '1' },
        { name: 'Company', value: '2' }
    ];

    return (

    <div class="container">
        <br></br>
        <ButtonGroup>
        {usersRoles.map((role, idx) => (
          <ToggleButton
            key={idx}
            id={`role-${idx}`}
            type="radio"
            variant={idx % 2 ? 'outline-primary' : 'outline-primary'}
            name="role"
            value={role.value}
            checked={selectedRole === role.value}
            onChange={(e) => {setSelectedRole(e.currentTarget.value); console.log(e.currentTarget.value)}}
          >
            {role.name}
          </ToggleButton>
        ))}
      </ButtonGroup>
      {selectedRole === '1' ? (<ConsumerSignUp />) : (<CompanySignUp />) }
    </div>
    )
}

export default SignUpPage;