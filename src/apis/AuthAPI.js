import axios from "axios";
import TokenManager from "./TokenManager";
import API_BASE_URL from "./Constants";

const LOGIN_URL = API_BASE_URL + "/login";

const AuthAPI = {
    login: (email, password) => axios.post(LOGIN_URL, { email, password })
        .then(response => response.data.accessToken)
        .then(accessToken => TokenManager.setAccessToken(accessToken))
}

export default AuthAPI;