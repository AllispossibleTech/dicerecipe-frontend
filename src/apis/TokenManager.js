import jwt_decode from "jwt-decode";

// const userData = {
//     accessToken: undefined,
//     claims: undefined
// }

const TokenManager = {
    getAccessToken: () => localStorage.getItem("token"),
    getClaims: () => {
        if (!localStorage.getItem("token")) {
            return undefined;
        }
        return jwt_decode(localStorage.getItem("token"));
    },
    setAccessToken: (token) => {
        localStorage.setItem("token", token);
        // userData.accessToken = token;
        const claims = jwt_decode(token);
        localStorage.setItem("sub", claims.sub);
        localStorage.setItem("role", claims.role);
        localStorage.setItem("roleId", claims.roleId);
        localStorage.setItem("userId", claims.userId);
        // userData.claims = claims;
        return claims;
    },
    clear: () => {
        // userData.accessToken = undefined;
        // userData.claims = undefined;
        localStorage.removeItem("token");
        localStorage.removeItem("sub");
        localStorage.removeItem("role");
        localStorage.removeItem("roleId");
        localStorage.removeItem("userId");
    }
}

export default TokenManager;