import axios from "axios"; 
import TokenManager from "./TokenManager";
import API_BASE_URL from "./Constants";

const PRODUCTS_BASE_URL = API_BASE_URL+ "/products"; 

const ProductsApi = { 
    getProducts: () => axios.get(PRODUCTS_BASE_URL) 
        .then(response => response.data)
        .catch(error => { console.log(error);} ), 
    getProductsByCompanyDetailsId: (detailsId) => axios.get(PRODUCTS_BASE_URL + "/" + detailsId, 
    {
        headers: { Authorization: `Bearer ${TokenManager.getAccessToken()}` }
    }) 
        .then(response => response.data)
        .catch(error => { console.log(error);} ), 
    createProduct: (companyId, name, unit) => axios.post(PRODUCTS_BASE_URL, 
        {
            companyId,
            product: {
                id: 0,
                name,
                unit
            }
        },
        {
            headers: { Authorization: `Bearer ${TokenManager.getAccessToken()}`}
        }) 
        .then(response => response.data)
        .catch(error => { console.log(error);} ),
    getSearchedProducts: (searchQuery) => axios.get(PRODUCTS_BASE_URL + "/search/" + searchQuery)
        .then(response => response.data)
        .then(console.log(searchQuery))
        .catch(error => { console.log(error);} ),
    delete: (product) => axios.post(PRODUCTS_BASE_URL + "/delete", product, 
    {
        headers: { Authorization: `Bearer ${TokenManager.getAccessToken()}`}
    })
        .then(response => response.data)
        .catch(error => { console.log(error);} ) 
}; 

export default ProductsApi;