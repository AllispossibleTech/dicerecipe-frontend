import axios from "axios"; 
import TokenManager from "./TokenManager";
import API_BASE_URL from "./Constants";

const RECIPES_BASE_URL = API_BASE_URL+"/recipes"; 

const RecipesApi = { 
    getRecipes: () => axios.get(RECIPES_BASE_URL) 
        .then(response => response.data)
        .catch(error => { console.log(error);} ), 
    getRecipe: (recipeId) => axios.get(RECIPES_BASE_URL + "/" + recipeId)
        .then(response => response.data)
        .catch(error => { console.log(error);} ), 
    getRecipesByCompanyDetailsId: (detailsId) => axios.get(RECIPES_BASE_URL + "/company/" + detailsId, 
        {
            headers: { Authorization: `Bearer ${TokenManager.getAccessToken()}` }
        }) 
            .then(response => response.data)
            .catch(error => { console.log(error);} ), 
    createRecipe: (recipe) => axios.post(RECIPES_BASE_URL, recipe, 
        {
            headers: { Authorization: `Bearer ${TokenManager.getAccessToken()}` }
        }) 
        .then(response => response.data),
    loadSuggestions: (consumerId) => axios.get(RECIPES_BASE_URL + "/loadsuggestions/" + consumerId, 
        {
            headers: { Authorization: `Bearer ${TokenManager.getAccessToken()}` }
        }) 
        .then(response => response.data)
        .catch(error => { console.log(error);} ),
    getTimeToPrepare: (time) => axios.get(RECIPES_BASE_URL + "/time-to-prepare/" + time, 
        {
            headers: { Authorization: `Bearer ${TokenManager.getAccessToken()}` }
        }) 
        .then(response => response.data)
        .catch(error => { console.log(error);} ),
    getTimeToPrepareRange: (min, max) => axios.get(RECIPES_BASE_URL + "/time-to-prepare-range", 
        {
            params: {
                min,
                max
            }
        }) 
        .then(response => response.data)
        .catch(error => { console.log(error);} ),
    delete: (recipe) => axios.post(RECIPES_BASE_URL + "/delete", recipe, 
        {
            headers: { Authorization: `Bearer ${TokenManager.getAccessToken()}`}
        })
            .then(response => response.data)
            .catch(error => { console.log(error);} ) 
}; 

export default RecipesApi;