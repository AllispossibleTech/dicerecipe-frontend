import axios from "axios";
import TokenManager from "./TokenManager";
import API_BASE_URL from "./Constants";

const COMPANY_URL = API_BASE_URL + "/companies";

const CompanyAPI = {
    getCompany: (companyId) => axios.get(COMPANY_URL+`/${companyId}`,
        {
            headers: { Authorization: `Bearer ${TokenManager.getAccessToken()}` }
        })
        .then(response => response.data)
        .catch(error => console.log(error)),
    createCompany: (company) => axios.post(COMPANY_URL, company)
        .then(response => response.data)
        .catch(error => console.log(error))
}

export default CompanyAPI;