import axios from "axios";
import TokenManager from "./TokenManager";
import API_BASE_URL from "./Constants";

const CONSUMER_URL = API_BASE_URL + "/consumers";

const ConsumerAPI = {
    getConsumer: (consumerId) => axios.get(CONSUMER_URL+`/${consumerId}`,
        {
            headers: { Authorization: `Bearer ${TokenManager.getAccessToken()}` }
        })
        .then(response => response.data)
        .catch(error => console.log(error)),
    createConsumer: (consumer) => axios.post(CONSUMER_URL, consumer)
        .then(response => response.data)
        .catch(error => console.log(error)),
    addProductToFoodInventory: (productId, consumerId) => axios.post(CONSUMER_URL+'/addproduct', 
    { 
        productId, 
        consumerId
    },
    {
        headers: { Authorization: `Bearer ${TokenManager.getAccessToken()}`}
    })
        .then(console.log("ProductId "+productId + " consumerId "+consumerId))
        .then(response => response.data),
    
    removeProductFromFoodInventory: (productId, consumerId) => axios.post(CONSUMER_URL+'/removeproduct', 
    { 
        productId, 
        consumerId
    },
    {
        headers: { Authorization: `Bearer ${TokenManager.getAccessToken()}`}
    })
        .then(console.log("ProductId "+productId + " consumerId "+consumerId))
        .then(response => response.data),

    updateTime: (minutes, consumerId) => axios.post(CONSUMER_URL+'/update-time', 
        { 
            minutes, 
            consumerId
        },
        {
            headers: { Authorization: `Bearer ${TokenManager.getAccessToken()}`}
        })
            .then(response => response.data)
            .catch(error => console.log(error))

}

export default ConsumerAPI;