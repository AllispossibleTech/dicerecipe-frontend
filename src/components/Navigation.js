import '../App.css';
import { Navbar, Container, Nav, NavDropdown } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import AuthAPI from "../apis/AuthAPI";
import { useEffect } from "react";
import TokenManager from '../apis/TokenManager';


function Navigation(props) {

    const handleLogout = () => {
        TokenManager.clear();
        props.setClaims(null);
    }

    useEffect(() => {
    }, [props.claims]);

    const renderPage = () => {
      if (props.claims && props.claims?.role == "CONSUMER") {
        return(
          <Navbar bg="light" expand="lg">
            <Container>
              <Navbar.Brand href="/">DiceRecipe</Navbar.Brand>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                  <Nav className="me-auto">
                      <Nav.Link href="/">Home</Nav.Link>
                      <Nav.Link href="/products">Products</Nav.Link>
                      <Nav.Link href="/recipes">Recipes</Nav.Link>
                      <Nav.Link href="/notifications">Notifications</Nav.Link>
                      <Nav.Link onClick={handleLogout}>Logout</Nav.Link>
                  </Nav>
                </Navbar.Collapse>
            </Container>
          </Navbar>
        )
      } else if (props.claims && props.claims?.role == "COMPANY") {
        return(
          <Navbar bg="light" expand="lg">
            <Container>
              <Navbar.Brand href="/">DiceRecipe</Navbar.Brand>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                  <Nav className="me-auto">
                      <Nav.Link href="/">Home</Nav.Link>
                      <Nav.Link href="/products">Products</Nav.Link>
                      <Nav.Link href="/recipes">Recipes</Nav.Link>
                      <Nav.Link href="/create-recipe">Create Recipe</Nav.Link>
                      <Nav.Link onClick={handleLogout}>Logout</Nav.Link>
                  </Nav>
                </Navbar.Collapse>
            </Container>
          </Navbar>
        )
      } else {
        return(
          <Navbar bg="light" expand="lg">
            <Container>
                <Navbar.Brand href="/">DiceRecipe</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                        <Nav.Link href="/">Home</Nav.Link>
                        <Nav.Link href="/products">Products</Nav.Link>
                        <Nav.Link href="/recipes">Recipes</Nav.Link>
                        <Nav.Link href="/login">Login</Nav.Link>
                        <Nav.Link href="/signup">SignUp</Nav.Link>
                    </Nav>
              </Navbar.Collapse>
            </Container>
          </Navbar>
        )
      }
    }

  return (
    <div>
    {renderPage()}
    
      </div>
  );
}

export default Navigation;
