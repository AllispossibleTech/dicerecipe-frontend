import ProductsApi from "../apis/ProductsApi";
import React, { useState } from "react";
import TokenManager from "../apis/TokenManager";

function AddProductForm(props) {

    const [name, setName] = useState("");
    const [unit, setUnit] = useState("");
    const [description, setDescription] = useState("");

    const resetForm = () => {
        setName("");
        setUnit("");
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        await ProductsApi.createProduct(props.id, name, unit);
        props.handleAddProduct();
        console.log("Add Product Form Submitted")
        resetForm();
    }

    return(
        <div>
            <h2>Add Product</h2>
            <form onSubmit={handleSubmit}>
                <div className="mb-3">
                    <input type="text" placeholder="Name" className="form-control" id="productName"  onChange={(e) => setName(e.target.value)} value={name}/>
                </div>
                <div className="mb-3">
                    <input type="text" placeholder="Unit" className="form-control" id="productUnit" onChange={(e) => setUnit(e.target.value)} value={unit}/>
                </div>
                <button type="submit" className="btn btn-primary">Add</button>
            </form>
        </div>
    )
}

export default AddProductForm;

