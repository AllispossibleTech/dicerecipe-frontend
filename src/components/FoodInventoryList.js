import React, { useState } from "react";
import FoodInventoryItem from "./FoodInventoryItem";

function FoodInventoryList(props) {

    return (
        <div class="container">
            <ul className="list-group mt-3">
                {props.products.map(product => (
                    <FoodInventoryItem key={product.id} product={product} removeFromInventory={props.removeFromInventory}/>
                ))}
            </ul>
        </div>
    )
}

export default FoodInventoryList;