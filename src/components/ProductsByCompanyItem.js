import React, { useState, useEffect } from "react";

function ProductsByCompanyItem(props) {
    const name = props.name;
    const handleDelete = () => {
      props.handleDelete(props.product);
    }

    return (
        <li className="list-group-item d-flex justify-content-between align-items-center">
            {name}
            <button className="btn btn-primary btn-sm" onClick={handleDelete}>Delete</button>   
        </li>
    )
}

export default ProductsByCompanyItem;