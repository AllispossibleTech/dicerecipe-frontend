import React, { useState } from "react";
import ConsumerAPI from "../apis/ConsumerAPI";
import TokenManager from "../apis/TokenManager";

function FoodInventoryItem(props) {

    const handleDelete = () => {
        props.removeFromInventory(props.product.id);
    }

    return (

        <li className="list-group-item d-flex justify-content-between align-items-center" key={props.product.id}>
            {props.product.name}
            <button className="btn btn-primary btn-sm" onClick={handleDelete}>Remove</button>    
        </li>
    )
}

export default FoodInventoryItem;