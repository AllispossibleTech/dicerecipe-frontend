import React, { useState, useEffect } from "react";
import RecipesApi from "../apis/RecipesApi";
import { useNavigate } from "react-router-dom";

function SuggestedRecipesList(props) {
  const [suggestedRecipes, setSuggestedRecipes] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    const fetchSuggestions = async () => {
      try {
        const recipes = await RecipesApi.loadSuggestions(props.consumerId);
        console.log(recipes);
        setSuggestedRecipes(recipes);
      } catch (error) {
        console.log(error);
      }
    };

    fetchSuggestions();
  }, [props.consumerId]);

  return (
    <div className="container">

      <ul className="list-group mt-3">
      {suggestedRecipes.map((recipe) => (
        <li className="list-group-item d-flex justify-content-between align-items-center" key={recipe.id}>
          {recipe.title}
          <button className="btn btn-primary btn-sm" onClick={() => navigate(`/recipes/${recipe.id}`)}>View</button>
        </li>
      ))}
      </ul>
    </div>
    
  );
}

export default SuggestedRecipesList;