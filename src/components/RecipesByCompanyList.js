import React, { useState, useEffect } from "react";
import RecipesByCompanyItem from "./RecipesByCompanyItem";
import RecipesApi from "../apis/RecipesApi";

function RecipesByCompanyList(props) {

    const [recipes, setRecipes] = useState([]);

    useEffect(() => {
        const fetchSuggestions = async () => {
          try {
            const recipes = await RecipesApi.getRecipesByCompanyDetailsId(props.detailsId);
            setRecipes(recipes);
          } catch (error) {
            console.log(error);
          }
        };
    
        fetchSuggestions();
      }, [props.detailsId]);

    const handleDelete = async (recipe) => {
      await RecipesApi.delete(recipe);
      props.getUserDetails();
      window.location.reload(false);
    }

    return (
        <div class="container">
            <ul className="list-group mt-3">
                {recipes.map(recipe => (
                    <RecipesByCompanyItem key={recipe.id} recipe={recipe} handleDelete={handleDelete} />
                ))}
            </ul>
        </div>
    )
}

export default RecipesByCompanyList;