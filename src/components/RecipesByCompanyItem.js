import React, { useState, useEffect } from "react";
import { Modal, Button } from "react-bootstrap";

function RecipesByCompanyItem(props) {
    const [show, setShow] = useState(false);

    const handleClose = () => {setShow(false);}
    const handleShow = () => {
        setShow(true);
      }

    const handleDelete = () => {
      props.handleDelete(props.recipe);
    }

    return (
        <>
        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Deleting Recipe..</Modal.Title>
            </Modal.Header>
            <Modal.Body>Are you sure you want to delete this recipe?</Modal.Body>
            <Modal.Footer>
                <Button variant="danger" onClick={handleDelete}>
                Confirm Delete
                </Button>
                <Button variant="secondary" onClick={handleClose}>
                Cancel
                </Button>
            </Modal.Footer>
        </Modal>
        <li className="list-group-item d-flex justify-content-between align-items-center">
            {props.recipe.title}
            <button className="btn btn-primary btn-sm" onClick={handleShow}>Delete</button>   
        </li>
    </>
    )
}

export default RecipesByCompanyItem;