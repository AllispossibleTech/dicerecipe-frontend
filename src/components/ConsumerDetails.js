import {useState} from "react";
import ConsumerAPI from "../apis/ConsumerAPI";

const ConsumerDetails = ({ consumerDetails }) => {
    const [minutes, setMinutes] = useState(consumerDetails.preferredCookingTime)

    const handleUpdate = async () => {
        await ConsumerAPI.updateTime(minutes, consumerDetails.id)
        window.location.reload();
    }

    return (
        <div>
            <ul className="list-group mt-3">
                {console.log(consumerDetails)}
                <li className="list-group-item d-flex justify-content-between align-items-center"><b>Name:</b> {consumerDetails.account.name}</li>
                <li className="list-group-item d-flex justify-content-between align-items-center"><b>Consumer ID:</b> {consumerDetails.id}</li>
                <li className="list-group-item d-flex justify-content-between align-items-center"><b>Account ID:</b> {consumerDetails.account.id}</li>
                <li className="list-group-item d-flex justify-content-between align-items-center"><b>Email:</b> {consumerDetails.account.email}</li>
                <li className="list-group-item d-flex justify-content-between align-items-center"><b>Preferred Cooking Time:</b> <br></br><input type="number" className="form-control" style={{width:"80px"}} placeholder="Minutes" value={minutes} onChange={event => setMinutes(event.target.value)}/> minutes</li> 
                <button className="btn btn-secondary btn-sm float-right" onClick={handleUpdate}>Update Time</button>
            </ul>
        </div>
    );
};

export default ConsumerDetails;