import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import CompanyAPI from "../apis/CompanyAPI";

function CompanySignUp() {

    const navigate = useNavigate();

    // Common For both user roles
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    // Specific to companies
    const [address, setAddress] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [vatNumber, setVatNumber] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();

        const company = {
            "id": 0,
            "details": {
                "id": 0,
                "address": address,
                "name": companyName,
                "vatNumber": vatNumber
            },
            "account": {
                "id": 0,
                "name": name,
                "email": email,
                "password": password,
                "userType": "COMPANY"
            }
        }

        await CompanyAPI.createCompany(company).then(navigate("/login"))
        
    }


    return (

    <div class="container">
        <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card border-0 shadow rounded-3 my-5">
            <div class="card-body p-4 p-sm-5">
                <h5 class="card-title text-center mb-5 fw-light fs-5">Sign Up</h5>
                <form onSubmit={handleSubmit}>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="floatingInput" placeholder="Username" onChange={(e) => setName(e.target.value)} value={name}/>
                    <label for="floatingInput">Account Name</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="email" class="form-control" id="floatingInput" placeholder="name@example.com" onChange={(e) => setEmail(e.target.value)} value={email}/>
                    <label for="floatingInput">Email address</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="password" class="form-control" id="floatingPassword" placeholder="Password" onChange={(e) => setPassword(e.target.value)} value={password} />
                    <label for="floatingPassword">Password</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="floatingInput" placeholder="Brand B.V." onChange={(e) => setCompanyName(e.target.value)} value={companyName}/>
                    <label for="floatingInput">Company Name</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="floatingInput" placeholder="Nice Street 14, 1234AB NL" onChange={(e) => setAddress(e.target.value)} value={address}/>
                    <label for="floatingInput">Address</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="floatingInput" placeholder="0123456789" onChange={(e) => setVatNumber(e.target.value)} value={vatNumber}/>
                    <label for="floatingInput">Vat Number</label>
                </div>
                <div class="d-grid">
                    <button class="btn btn-primary btn-login text-uppercase fw-bold" type="submit">Signup</button>
                </div>
                <hr class="my-4" />
                </form>
            </div>
            </div>
        </div>
        </div>
    </div>
    )
}

export default CompanySignUp;