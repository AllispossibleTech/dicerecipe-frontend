import React, { useState, useEffect } from "react";
import ProductsApi from "../apis/ProductsApi";
import ProductsByCompanyItem from "./ProductsByCompanyItem";

function ProductsByCompanyList(props) {

    const [products, setProducts] = useState([]);

    useEffect(() => {
        const fetchSuggestions = async () => {
          try {
            const products = await ProductsApi.getProductsByCompanyDetailsId(props.detailsId);
            console.log(products);
            setProducts(products);
          } catch (error) {
            console.log(error);
          }
        };
    
        fetchSuggestions();
      }, [props.detailsId]);

    const handleDelete = async (product) => {
      const response = await ProductsApi.delete(product);
      if (response == "Product cannot be deleted because it's part of a Recipe as an ingredient!") {
        alert(response);
      }
      props.getUserDetails();
      window.location.reload(false);
    }

    return (
        <div class="container">
            <ul className="list-group mt-3">
                {products.map(product => (
                    <ProductsByCompanyItem product={product} handleDelete={handleDelete} name={product.name} key={product.id} />
                ))}
            </ul>
        </div>
    )
}

export default ProductsByCompanyList;