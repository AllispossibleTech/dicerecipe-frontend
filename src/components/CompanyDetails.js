const CompanyDetails = ({ companyDetails }) => {
    return (
        <div>
            <ul className="list-group mt-3">
                {console.log(companyDetails)}
                <li className="list-group-item d-flex justify-content-between align-items-center"><b>Brand Name:</b> {companyDetails.account.name}</li>
                <li className="list-group-item d-flex justify-content-between align-items-center"><b>Company Name:</b> {companyDetails.details.name}</li>
                <li className="list-group-item d-flex justify-content-between align-items-center"><b>Address:</b> {companyDetails.details.address}</li>
                <li className="list-group-item d-flex justify-content-between align-items-center"><b>VAT Code:</b> {companyDetails.details.vatNumber}</li>
                <li className="list-group-item d-flex justify-content-between align-items-center"><b>Company ID:</b> {companyDetails.id}</li>
                <li className="list-group-item d-flex justify-content-between align-items-center"><b>Account ID:</b> {companyDetails.account.id}</li>
                <li className="list-group-item d-flex justify-content-between align-items-center"><b>Details ID:</b> {companyDetails.details.id}</li>
                <li className="list-group-item d-flex justify-content-between align-items-center"><b>Email:</b> {companyDetails.account.email}</li>
            </ul>
        </div>
    );
};

export default CompanyDetails;